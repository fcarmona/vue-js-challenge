import {mutations} from './store'

let initialState

beforeEach(() => {
  initialState = {
    todos: []
  }
})

describe('TodoStore', () => {
  it('addTodo', () => {
    const testTodo = {
      text: 'addTodoTest',
      checked: true,
      createdAt: new Date()
    }
    mutations.addTodo(initialState, testTodo)

    expect(initialState).toEqual({
      todos: [
        testTodo
      ]
    })
  })

  it('removeTodo', () => {
    const state = {
      todos: [
        {
          text: 'removeTodoTest',
          checked: true,
          createdAt: new Date()
        },
        {
          text: 'removeTodoTest1',
          checked: false,
          createdAt: new Date()
        }
      ]
    }
    mutations.removeTodo(state, 1)

    expect(state.todos).toEqual([
      {
        text: 'removeTodoTest',
        checked: true,
        createdAt: new Date()
      }
    ])
  })
})
